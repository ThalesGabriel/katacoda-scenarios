&nbsp;

## Agora á a hora de conectarmos nosso webhook, entre na sua página de perfil do katacoda e clique no ícone de `ferramentas`

&nbsp;&nbsp;

## No campo de texto:

> Git Scenario Repository

&nbsp;&nbsp;

Vá ao github e clique no botão `clone` no canto superior direito, selecione o modo `https` e após o clone copie a url mostrada do tipo https://username@bitbucket.org/username/cenario.git

&nbsp;&nbsp;

Volte ao perfil do `katacoda` e cole a url que vc copiou no campo de input 

> Git Scenario Repository

&nbsp;&nbsp;

Clique em `save` e depois `Trigger Webhook`